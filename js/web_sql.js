let runoobDB = openDatabase(
  'runoobDB',
  '1.0',
  'test db',
  2 * 1024 * 1024
);
runoobDB.transaction(function (tx) {
  tx.executeSql('create table if not exists test_db (id unique, log)');
  tx.executeSql('insert into test_db (id, log) values (1, "runoob1")');
  tx.executeSql('insert into test_db (id, log) values (2, "runoob2")');
});

let db = openDatabase(
  'test_db',
  '1.0',
  'Test DB',
  2 * 1024 * 1024
);
db.transaction(function (tx) {
  tx.executeSql(`create table if not exists student (student_id integer primary key, age, score)`);
  tx.executeSql(`insert into student (age, score) values (18, 87)`);
});
