console.log('worker.js start');

const fibonacci = num => num * 2;
onmessage = event => {
  let num = event.data;
  let result = fibonacci(num);
  postMessage(result);
};