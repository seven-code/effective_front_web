class Dog {
  constructor(age) {
    this.age = age;
  }

  bark() {
    console.log('wang wang');
  }

  bite(man) {
    console.log(`bite ${man}`);
  }
}

class Husky extends Dog {
  constructor(age) {
    super(age);
  }

  bark() {
    console.log('wolf wolf');
  }
}

let smallHusky = new Husky(2);

let age = smallHusky.age;
console.log(age);
smallHusky.bark();