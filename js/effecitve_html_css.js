(function () {
  var searchForm = document.getElementById('searchForm');
  var searchFormSubmit = document.getElementById('searchFormSubmit');
  searchFormSubmit.addEventListener('click', function (event) {
    var username = searchForm.username.value;
    var password = searchForm.password.value;
    if (username && password) {
      searchFormSubmit.onsubmit();
    } else {
      alert('请输入用户名和密码！');
      event.preventDefault();
    }
  }, false)
})();